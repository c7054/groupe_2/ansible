# ANSIBLE BUILD
Documentation : https://docs.ansible.com/ansible/latest/

## Présentation de ansible
- Crée en 2012 utilisant le langage Python
- Logiciel libre de gestion des configurations par des livraisons continue des mises à jour
- Infrastructure as Code (IaS)
- Déploiement via SSH

## Cas concret
Concrètement Ansible va permettre de :
- Déployer des configurations sur un serveur distants
- Installer/désinstaller, lancer/stopper/relancer, activer/désactiver un service système

## Exercice Ansible
Exercice 1 : Mettre en place via Ansible : Installer Nginx si aucun service web n'est installé, dans le cas où Nginx est déjà installé, supprimez Nginx et installez Apache
Exercice 2 : Intégrer Ansible dans la configuration Packer

## Pré-requis
Une VM fonctionnelle (Debian 10 ou 11) ayant internet et accessible en SSH par le groupe.
Un accès à Azure

## Comment télécharger ANSIBLE
Pré-requis:
- Binaire ansible-playbook
- Pouvoir exécuter du python

## Installation d'ANSIBLE
- Exemple sous CentOS :
```$ sudo yum install ansible```

- Exemple sous Debian :
```
$ sudo apt-get install software-properties-common
$ sudo apt-add-repository ppa:ansible/ansible
$ sudo apt-get update
$ sudo apt-get install ansible
```

# ANSIBLE RUN
L'utilisation d'ansible se fait via la création de rôles qui va décrire les attentes de déploiement sur un ou plusieurs serveurs distants. Une fois les rôles développés, la création de "playbooks" permet de choisir les différents rôles que l'on veut utiliser et déployer sur le serveur distant.

Pré-requis:
- SSH-pass installé sur le serveur ANSIBLE
- Python doit être installé sur le serveur distant

On retient donc la partie :
- Rôle (développement des configurations; exemple : déployer SSH)
- Playbook (appel des rôles développés; exemple : utiliser le rôle SSH sur le serveur x.x.x.x)

## Rôle
### Initialisation d'un rôle contenant les configurations
Création d'un rôle avec ansible galaxy :
```
ansible-galaxy init $nom du rôle$
```

### Arborescence du rôle
```
- defaults : variables par défaut pour le rôle
- files : contient des fichiers qui peuvent être déployés via ce rôle
- handlers : contient les handlers, qui peuvent être utilisés par ce rôle ou même en dehors de ce rôle
- meta : définit certaines métadonnées pour ce rôle
- README.md : inclut une description générale du fonctionnement de votre rôle
- tasks : contient la liste principale des tâches à exécuter par le rôle
- template : contient des modèles (jinja2) qui peuvent être déployés via ce rôle.
- tests : contient notre playbook (on peut cependant déposer notre playbook à la racine du projet ou dans un dossier sous un nom différent).
- vars : variables associées avec ce rôle
```

### Tasks
Installation d'un service :
```
---
# tasks file for roles/apache
- name: install apache  ## Description de la tâche
  apt:                 ## Utilisation du gestionnaire de paquet apt
    name: apache2       ## Nom du service à installer
    state: latest      ## Installer la dernière version disponible
```

## Playbook
Un playbook sert à décrire ce que nous attendons de notre déploiement sur un ensemble d'hôte distants

### Exemple de playbook
```
---
- hosts: all
  remote_user: root
  roles:
    - roles/apache
```
Pour lancer le playbook, on utilise la commande suivante :
`ansible-playbook $NomDuPlaybook$ -i $INVENTAIRE `

Ce playbook va déployer toutes les tâches décrite dans le rôle apache soit l'installation du service dans sa dernière version.
## Linter
Il est possible d'utiliser un binaire nommé ansible-lint pour tester son code développé dans le playbook.
Pour cela nous pouvons installer directement le packet via apt (ou autre gestionnaire)
`apt install ansible-lint`

 Le projet est open-source et disponible sur github : https://github.com/ansible-community/ansible-lint

 ### Exemple error de syntax
 ```
 /mariadb/tasks$ ansible-lint main.yml
Syntax Error while loading YAML.
  did not find expected '-' indicator

The error appears to have been in '[...]/groupe3/ansible/roles/mariadb/tasks/main.yml': line 15, column 9, but may
be elsewhere in the file depending on the exact syntax problem.

The offending line appears to be:

      -  name: start mariadb
        service:
        ^ here
```
Il s'avère ici qu'il y a un espace en trop entre le '-' et 'name' (Un peu capricieux ansible sur l'indentation).
 Une fois corrigé le linter ne renvoie plus d'erreur.

 ## Exemple optimisation
 Le linter peut nous informer sur les bonnes pratiques à suivre sur le code développé.

 ```
 .../mariadb/tasks$ ansible-lint main.yml
[403] Package installs should not use latest
main.yml:9
Task/Handler: install mariadb
```

Ici le linter nous informe de ne pas spécifier de chercher la dernière version de mariadb. Suite au changement d'état, le linter ne renvoie plus de recommendation.

Pour de futures évolutions, ce linter pourrait être utilisé dans la CI pour réaliser des tests unitaires lors de déploiement de code dans le repository.

## Intégrer Ansible dans la configuration de Packer
Docs: https://www.packer.io/plugins/provisioners/ansible/ansible
Ansible peut être intégré comme "provisioners" dans la configuration de Packer.
Attention, il existe deux types de providers ansible (remote et local).

## Exemple 
```
provisioner "ansible" {
    playbook_file = "playbook.yml"
```

## RUN 
Pour déployer, il suffit d'utiliser la commande d'execution de Packer.
`packer build path_to_packer_file`

## Droits des utilisateurs
Pour effectuer des commandes en tant que sudo sur la machine distante, il peut être nécessaire d'utiliser le paramètre `become`
